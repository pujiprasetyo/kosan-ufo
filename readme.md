# Kosan Ufo

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Website ini merupakan tugas dari mata kuliah materi pengelolaan proyek perangkat lunak. Pada web ini menggunakan beberapa framework dan tools antara lain:

  - CodeIgniter
  - Materialize
  - Jquery
  - Database MySql

# Features!

  - Halaman awal yakni login untuk para admin 
  - Halaman dashboard merupakan view antara nomor kamar, nama penghuni dan pembayaran terakhir
  - Halaman daftar penyewa meruapakan kumpulan biodata penghuni kosan
  - Halaman pengaturan kamar untuk melakukan perubahan harga kamar ataupun ada penambahan atau pengurangan kamar
  - Halaman profil, tentang admin dan pengaturan password
