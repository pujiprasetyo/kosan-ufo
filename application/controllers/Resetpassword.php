<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Resetpassword extends CI_Controller {
 
    function __construct() {
        parent:: __construct();
		if ($this->session->has_userdata('username')) {
			redirect('site');
		}
		if (!empty($this->uri->segment(2))) {
			redirect('access-denied');
		}
		$this->load->model('M_Admin');
		$this->load->library('email');
    }
 
	function index() {
		$this->form_validation->set_error_delimiters('<div class="center delimiter">', '</div>');
		$this->form_validation->set_message('valid_email', 'Email tidak benar');
		$this->form_validation->set_rules('email','Email Address','required|valid_email');
		
		if($this->form_validation->run() == FALSE) {
			$this->load->view('header');
			$this->load->view('reset_password');
			$this->load->view('footer');
		} else {
			$email = $this->input->post('email');
			$data['admin'] = $this->M_Admin->readUser(null, null, $email);
			if (!empty($data['admin']->email)) {
				if ($data['admin']->email === $email) {
					$this->sendMail();
				}
			} else {
				$pesan = "<script>Materialize.toast('Email yang anda masukkan salah', 4000, 'red rounded')</script>";
				$this->session->set_flashdata('toast', $pesan);
				redirect('resetpassword');
			}
		}
	}
 
	function generateRandomString($length = 7) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
 
    function sendMail() {
		$passwordBaru = $this->generateRandomString();
		$emailTujuan = $this->input->post('email');
		$data['admin'] = $this->M_Admin->readUser(null, null, $emailTujuan);
		$username = $data['admin']->username;
		
		$this->load->library('konfigurasi_smtp');
		$config['protocol'] = $this->konfigurasi_smtp->protocol();
		$config['smtp_host'] = $this->konfigurasi_smtp->smtp_host();
		$config['smtp_port'] = $this->konfigurasi_smtp->smtp_port();
		$config['smtp_user'] = $this->konfigurasi_smtp->smtp_user(); // email gmail atau email domain
		$config['smtp_pass'] = $this->konfigurasi_smtp->smtp_pass(); // passwordnya
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $this->email->initialize($config);
        $this->email->from('kosanufo@kosan-ufo.000webhostapp.com', 'no-reply'); // pengirim
        $this->email->to($emailTujuan);
        $this->email->subject('Pemulihan Akun - KOSAN UFO'); // judul email
        $this->email->message('
			Aplikasi Sewa Kosan<br>
			KOSAN UFO<br><br>
			Berikut data akun anda<br><br> 
			Username : '.$username.'<br>
			Password : '.$passwordBaru.'
		'); 
        if ($this->email->send()) {
			$this->M_Admin->editPassword($passwordBaru, $emailTujuan);
			$pesan = "<script>Materialize.toast('Periksa email anda, untuk memulihkan akun !', 4000, 'red rounded')</script>";
			$this->session->set_flashdata('toast', $pesan);
			redirect('');
        } else {
            //show_error($this->email->print_debugger());
			echo "<h5>Maaf respon mail server sedang lambat, harap coba kembali.</h5>";
        }
    }
}