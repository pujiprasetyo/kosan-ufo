<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct() {
		parent:: __construct();
		if (!$this->session->has_userdata('username')) {
			redirect('');
		}
		$this->load->model('M_Admin');
		$this->load->library('email');
	}
	
	function index($username = FALSE) {
		$data['admin'] = $this->M_Admin->readUser();
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header');
			$this->load->view('data_admin', $data);
			$this->load->view('footer');
		} else {
			$this->M_Admin->editUser($username);
			$this->session->set_userdata('username', $this->input->post('nama'));
			$this->sendMail();
		}
	}
	
	function sendMail() {
		$name = $this->input->post('nama');
		$username = $this->input->post('username');
		$emailTujuan = $this->input->post('email');
		
		$this->load->library('konfigurasi_smtp');
		$config['protocol'] = $this->konfigurasi_smtp->protocol();
		$config['smtp_host'] = $this->konfigurasi_smtp->smtp_host();
		$config['smtp_port'] = $this->konfigurasi_smtp->smtp_port();
		$config['smtp_user'] = $this->konfigurasi_smtp->smtp_user(); // email gmail atau email domain
		$config['smtp_pass'] = $this->konfigurasi_smtp->smtp_pass(); // passwordnya
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		
		$this->email->initialize($config);
		$this->email->from('kosanufo@kosan-ufo.000webhostapp.com', 'no-reply'); // pengirim
        $this->email->to($emailTujuan);
        $this->email->subject('Pembaharuan Akun - KOSAN UFO'); // judul email
        $emailTujuan2 = str_replace(".", " ", $emailTujuan);
        $this->email->message('
			Aplikasi Sewa Kosan<br>
			KOSAN UFO<br><br>
			Data akun anda telah diperbaharui<br><br> 
			Nama : '.$name.'<br>
			Username : '.$username.'<br>
			Email : '.$emailTujuan2.'
		');
        if ($this->email->send()) {
			$pesan = "<script>Materialize.toast('Data admin berhasil diubah<br><br>Cek email anda SEKARANG !', 4000, 'red rounded')</script>";
			$this->session->set_flashdata('toast', $pesan);
			redirect('account');
        } else {
            //show_error($this->email->print_debugger());
			echo "<h5>Maaf respon mail server sedang lambat, harap coba kembali.</h5>";
        }
    }
	
	function editPassword() {
		$data['admin'] = $this->M_Admin->readUser();
		
		$this->form_validation->set_message('matches', 'Password baru tidak sama.');
		$this->form_validation->set_rules('password_lama', 'Password Lama', 'required|callback_passwordLama');
		$this->form_validation->set_rules('password_baru', 'Password Baru', 'required');
		$this->form_validation->set_rules('konfirmasi_password_baru', 'Konfirmasi Password Baru', 'required|matches[password_baru]');
		
		if ($this->form_validation->run() == FALSE) {
			$errors = form_error('konfirmasi_password_baru');
			$pesan = "<script>
				var pesan_flash = '<?= $errors';
				Materialize.toast(pesan_flash, 4000, 'red rounded')
				</script>";
			if ($errors != "") {
				$this->session->set_flashdata('toast', $pesan);
				redirect('account');
			}
			$this->load->view('header');
			$this->load->view('data_admin', $data);
			$this->load->view('footer');
		} else {
			$this->M_Admin->editPassword();
			$pesan = "<script>Materialize.toast('Password berhasil diubah', 4000, 'red rounded')</script>";
			$this->session->set_flashdata('toast', $pesan);
			redirect('account');
		}
	}
	
	function passwordLama() {
		$temp = md5($this->input->post('password_lama'));
		$data['admin'] = $this->M_Admin->readUser(null, $temp);
		if (!empty($data['admin']->password)) {
			if ($data['admin']->password === $temp) {
				return TRUE;
			}
		} else {
			$pesan = "<script>Materialize.toast('Password lama salah', 4000, 'red rounded')</script>";
			$this->session->set_flashdata('toast', $pesan);
			redirect('account');
		}
	}
	
	function logout() {
		if ($this->session->has_userdata('username')) {
			$this->session->sess_destroy();
			redirect('');
		} else {
			redirect('');
		}
	}
}