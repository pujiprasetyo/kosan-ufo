<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('M_Penyewa');
		if (!$this->session->has_userdata('username')) {
			redirect('');
		}
	}
	
	function index() {
		$data['penyewa'] = $this->M_Penyewa->listPenyewa();
		
		$this->load->view('header');
		$this->load->view('biodata_list', $data);
		$this->load->view('footer');
	}
	
	function notFound() {
		$this->load->view('header');
		$this->load->view('not_found');
		$this->load->view('footer');
	}
	
	function penyewa($id_penyewa = FALSE) {
		$nilai = FALSE;
		$data['penyewa'] = $this->M_Penyewa->readPenyewa($id_penyewa);
		if (empty($this->uri->segment(4))) {
			if (!empty($data['penyewa']->id_penyewa)) {
				$nilai = TRUE;
			}
		}
		if ($id_penyewa == TRUE && $nilai == TRUE) {
			$data['laporan'] = $this->M_Penyewa->readRiwayatPembayaran($id_penyewa);
			
			$this->load->view('header');
			$this->load->view('biodata', $data);
			$this->load->view('footer');
		} else {
			$this->notFound();
		}
	}
	
	function editData($id_penyewa = FALSE) {
		$nilai = FALSE;
		$data['penyewa'] = $this->M_Penyewa->readPenyewa($id_penyewa);
		$data['kamar'] = $this->M_Penyewa->readKamarKosong();
		if (empty($this->uri->segment(4))) {
			if (!empty($data['penyewa']->id_penyewa)) {
				$nilai = TRUE;
			}
		}
		if ($id_penyewa == TRUE && $nilai == TRUE) {
			$this->form_validation->set_rules('no_kamar', 'No Kamar', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('asal', 'Asal', 'required');
			$this->form_validation->set_rules('no_hp', 'No Hp', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('biodata_edit_data', $data);
				$this->load->view('footer');
			} else {
				$this->M_Penyewa->editData($id_penyewa);
				redirect('biodata/penyewa/'.$id_penyewa);
				//redirect('site');
			}
		} else {
			$this->notFound();
		}
	}
	
	function editPembayaran($id_penyewa = FALSE, $id_laporan = FALSE) {
		$nilai = FALSE;
		$data['laporan'] = $this->M_Penyewa->readEditRiwayatPembayaran($id_penyewa, $id_laporan);
		if (empty($this->uri->segment(5))) {
			if (!empty($data['laporan']->id_laporan)) {
				$nilai = TRUE;
			}
		}
		if ($id_penyewa == TRUE && $id_laporan == TRUE && $nilai == TRUE) {
			$this->form_validation->set_error_delimiters('<div class="center delimiter">', '</div>');
			$this->form_validation->set_message('required', 'Tanggal Pembayaran harus diisi.');
			$this->form_validation->set_rules('tgl_pembayaran', 'Tanggal Pembayaran', 'required');
			$this->form_validation->set_rules('harga_sewa', 'Harga Sewa', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('biodata_edit_pembayaran', $data);
				$this->load->view('footer');
			} else {
				$this->M_Penyewa->editPembayaran($id_laporan);
				redirect('biodata/penyewa/'.$id_penyewa);
			}
		} else {
			$this->notFound();
		}
	}
	
	function checkOut($id_penyewa = FALSE) {
		if ($id_penyewa == TRUE) {
			$data['penyewa'] = $this->M_Penyewa->readPenyewa($id_penyewa);
			if (!empty($data['penyewa']->id_penyewa)) {
				$no_kamar = $data['penyewa']->no_kamar;
			}
			$data['penyewa'] = $this->M_Penyewa->checkOut($id_penyewa, $no_kamar);
			redirect('biodata/penyewa/'.$id_penyewa);
		} else {
			$this->notFound();
		}
	}
}