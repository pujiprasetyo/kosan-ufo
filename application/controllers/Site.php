<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('M_Penyewa');
		if (!$this->session->has_userdata('username')) {
			redirect('');
		}
	}

	function index() {
		$data['penyewa'] = $this->M_Penyewa->readHome();
		
		$this->load->view('header');
		$this->load->view('home', $data);
		$this->load->view('footer');
	}
	
	function notFound() {
		$this->load->view('header');
		$this->load->view('not_found');
		$this->load->view('footer');
	}
	
	function verifikasi($id_penyewa = FALSE) {
		$nilai = FALSE;
		$data['penyewa'] = $this->M_Penyewa->readPenyewa($id_penyewa);
		if (empty($this->uri->segment(4))) {
			if (!empty($data['penyewa']->no_kamar)) {
				$nilai = TRUE;
			}
		}
		if ($id_penyewa == TRUE && $nilai == TRUE) {
			$data['harga_sewa'] = $this->M_Penyewa->readKamar($data['penyewa']->no_kamar);
			$harga_sewa = $data['harga_sewa']->harga_sewa;
			
			$this->form_validation->set_error_delimiters('<div class="center delimiter">', '</div>');
			$this->form_validation->set_message('required', 'Tanggal harus diisi.');
			$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('verifikasi', $data);
				$this->load->view('footer');
			} else {
				$this->M_Penyewa->bayarSewa($id_penyewa, $harga_sewa);
				redirect('');
			}
		} else {
			$this->notFound();
		}
	}
	
	function tambahPenyewa($no_kamar = FALSE) {
		$nilai = FALSE;
		$data['kamar'] = $this->M_Penyewa->readKamar($no_kamar);
		if (empty($this->uri->segment(4))) {
			if (!empty($data['kamar']->no_kamar)) {
				if ($data['kamar']->no_kamar == $no_kamar) {
					if ($data['kamar']->status != "aktif" ) {
						$nilai = TRUE;
					}
				}
			}
		}
		if ($no_kamar == TRUE && $nilai == TRUE) {
			$harga_sewa = $data['kamar']->harga_sewa;
			
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('asal', 'Asal', 'required');
			$this->form_validation->set_rules('no_hp', 'No Hp', 'required');
			
			/* 
			| validasi dengan php
			| jika terjadi inspect elemen pada halaman tambah penyewa
			*/
			$nama = $this->input->post('nama');
			if (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
				$nilai = FALSE;
			}
			
			if ($this->form_validation->run() == FALSE || $nilai == FALSE) {
				$this->load->view('header');
				$this->load->view('tambah_penyewa', $data);
				$this->load->view('footer');
			} else {
				$this->M_Penyewa->tambahPenyewa($no_kamar, $harga_sewa);
				redirect('');
			}
		} else {
			$this->notFound();
		}
	}
}
