<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('M_Admin');
		$this->load->library('email');
	}
	
	function index() {
		if ($this->session->has_userdata('username')) {
			redirect('site');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
			
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('login');
				$this->load->view('footer');
			} else {
				$data['admin'] = $this->M_Admin->readUser($this->input->post('username'), null, $this->input->post('username'));
				$this->session->set_userdata('username', $data['admin']->name);
				redirect('');
			}
		}
	}
	
	function passwordCheck() {
		$account = $this->M_Admin->cekPassword();
		if (!isset($account)) {
			$account = new \stdClass();
			$account->password = FALSE;
		}
		if (md5($this->input->post('password', TRUE)) === $account->password) {
			return TRUE;
		} else {
			$pesan = "<script>Materialize.toast('Username dan Password tidak sesuai', 4000, 'red rounded')</script>";
			$this->session->set_flashdata('toast', $pesan);
			redirect('');
		}
	}
}