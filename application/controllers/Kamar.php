<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('M_Penyewa');
		if (!$this->session->has_userdata('username')) {
			redirect('');
		}
	}
	
	function index() {
		$data['kamar'] = $this->M_Penyewa->listKamar();

		$this->load->view('header');
		$this->load->view('kamar_list', $data);
		$this->load->view('footer');
	}
	
	function notFound() {
		$this->load->view('header');
		$this->load->view('not_found');
		$this->load->view('footer');
	}
	
	function tambahKamar() {
		$nilai = FALSE;
		if (empty($this->uri->segment(3))) {
			$nilai = TRUE;
		}
		if ($nilai == TRUE) {
			$this->form_validation->set_error_delimiters('<div class="center delimiter">', '</div>');
			$this->form_validation->set_message('is_unique', 'Nomor Kamar tidak boleh sama.');
			$this->form_validation->set_rules('no_kamar','No Kamar','required|is_unique[tbl_kamar.no_kamar]');
			$this->form_validation->set_rules('harga_sewa','Harga Sewa','required');
			
			if($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('kamar_tambah');
				$this->load->view('footer');
			} else {
				$this->M_Penyewa->tambahKamar();
				redirect('kamar');
			}
		} else {
			$this->notFound();
		}
	}
	
	function editKamar($id_kamar = FALSE, $no_kamar = FALSE) {
		$nilai = FALSE;
		$data['kamar'] = $this->M_Penyewa->readKamar($no_kamar);
		if (empty($this->uri->segment(5))) {
			if (!empty($data['kamar']->id_kamar)) {
				if ($data['kamar']->id_kamar == $id_kamar && $data['kamar']->no_kamar == $no_kamar) {
					$nilai = TRUE;
				}
			}
		}
		if ($id_kamar == TRUE && $nilai == TRUE) {
			$this->form_validation->set_rules('harga_sewa','Harga Sewa','required');
			
			if($this->form_validation->run() == FALSE) {
				$this->load->view('header');
				$this->load->view('kamar_edit', $data);
				$this->load->view('footer');
			} else {
				echo $this->M_Penyewa->editKamar($id_kamar);
				redirect('kamar');
			}
		} else {
			$this->notFound();
		}
	}
	
	function hapusKamar($id_kamar) {
		$this->M_Penyewa->hapusKamar($id_kamar);
		redirect('kamar');
	}
}