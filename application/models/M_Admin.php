<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function cekPassword() {
		$this->db->select('password');
		$array = array(
			'username' => $this->input->post('username', TRUE),
			'email' => $this->input->post('username', TRUE)
		);
		$this->db->or_where($array);
		$query = $this->db->get('tbl_admin');
		return $query->row();
	}
	
	function readUser($username = FALSE, $password_lama = FALSE, $email = FALSE) {
		if ($username == TRUE && $email == TRUE) {
			$array = array(
				'username' => $this->input->post('username', TRUE),
				'email' => $this->input->post('username', TRUE)
			);
			$this->db->or_where($array);
			$query = $this->db->get('tbl_admin');
			//$query = $this->db->get_where('tbl_admin', array('username' => $username, 'email' => $email));
			return $query->row();
		}
		if ($username == TRUE) {
			$query = $this->db->get_where('tbl_admin', array('username' => $username));
			return $query->row();
		} else if ($password_lama == TRUE) {
			$query = $this->db->get_where('tbl_admin', array('password' => $password_lama));
			return $query->row();
		} else if ($email == TRUE) {
			$query = $this->db->get_where('tbl_admin', array('email' => $email));
			return $query->row();
		} else {
			$query = $this->db->get_where('tbl_admin');
			return $query->result_array();
		}
	}
	
	function editUser($username) {
		$data = array(
			'name' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email')
		);
		$this->db->where('username', $username);
		return $this->db->update('tbl_admin', $data);
	}
	
	function editPassword($passwordBaru = FALSE, $email = FALSE) {
		if ($passwordBaru == TRUE && $email == TRUE) {
			$data = array(
				'password' => md5($passwordBaru)
			);
			$this->db->where('email', $email);
			return $this->db->update('tbl_admin', $data);
		} else {
			$data = array(
				'password' => md5($this->input->post('password_baru'))
			);
			$this->db->where('password', md5($this->input->post('password_lama')));
			return $this->db->update('tbl_admin', $data);
		}
		
	}
}
