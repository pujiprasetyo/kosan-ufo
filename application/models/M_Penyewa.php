<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Penyewa extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	// select - tampilan pada halaman Home
	function readHome() {
		$this->db->select('tbl_penyewa.*, tbl_kamar.no_kamar');
		$this->db->from('tbl_kamar');
		$this->db->join('tbl_penyewa', 'tbl_penyewa.no_kamar = CAST(tbl_kamar.no_kamar as CHAR)', 'left');
		$this->db->order_by('tbl_kamar.no_kamar asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// select - kamar untuk penyewa baru
	function readKamar($no_kamar) {
		$query = $this->db->get_where('tbl_kamar', array('no_kamar' => $no_kamar));
		return $query->row();
	}
	
	// select - melihat kamar yang kosong untuk Pindah kamar -> halaman biodata_edit_data 
	function readKamarKosong() {
		$this->db->order_by('no_kamar asc');
		$query = $this->db->get_where('tbl_kamar', array('status' => 'kosong'));
		return $query->result_array();
	}
	
	// select - verifikasi pembayaran terakhir pada verifikasi.php
	function readPenyewa($id_penyewa) {
		$query = $this->db->get_where('tbl_penyewa', array('id_penyewa' => $id_penyewa));
		return $query->row();
	}
	
	// insert - tambah penyewa di tampilan home
	function tambahPenyewa($no_kamar, $harga_sewa) {
		$tgl_masuk = date('d M, Y');
		$id_penyewa = substr_replace($no_kamar, date('dmYHis'), 1, 0);
		
		$data = array(
			'id_penyewa' => $id_penyewa,
			'no_kamar' => $no_kamar,
			'nama' => $this->input->post('nama'),
			'asal' => $this->input->post('asal'),
			'no_hp' => $this->input->post('no_hp'),
			'tgl_masuk' => $tgl_masuk,
			'tgl_pembayaran' => $tgl_masuk
		);
		$this->db->insert('tbl_penyewa', $data);
		
		$data = array(
			'harga_sewa' => $this->input->post('harga_sewa'),
			'status' => "aktif"
		);
		$this->db->where('no_kamar', $no_kamar);
		$this->db->update('tbl_kamar', $data);
		
		$data = array(
			'id_laporan' => md5($id_penyewa.date('lMYHis')),
			'id_penyewa' => $id_penyewa,
			'tgl_pembayaran' => $tgl_masuk,
			'harga_sewa' => $this->input->post('harga_sewa'),
			'bulan' => date('M'),
			'tahun' => date('Y')
		);
		$this->db->where('id_penyewa', $id_penyewa);
		return $this->db->insert('tbl_laporan', $data); 
	}
	
	// update - proses verifikasi pembayaran
	function bayarSewa($id_penyewa, $harga_sewa) {
		$data = array(
			'id_laporan' => md5($id_penyewa.date('lMYHis')),
			'id_penyewa' => $id_penyewa,
			'tgl_pembayaran' => $this->input->post('tanggal'),
			'harga_sewa' => $harga_sewa,
			'bulan' => date('M'),
			'tahun' => date('Y')
		);
		$this->db->where('id_penyewa', $id_penyewa);
		$this->db->insert('tbl_laporan', $data);
		
		$data = array(
			'tgl_pembayaran' => $this->input->post('tanggal')
		);
		$this->db->where('id_penyewa', $id_penyewa);
		return $this->db->update('tbl_penyewa', $data);
	}
	
	// select - tampilan pada halaman Daftar Penyewa
	function listPenyewa() {
		$this->db->order_by('tgl_keluar desc');
		$this->db->order_by('no desc');
		$query = $this->db->get('tbl_penyewa');
		return $query->result_array();
	}
	
	// select - tampilan riwayat pembayaran pada biodata penyewa
	function readRiwayatPembayaran($id_penyewa) {
		$this->db->order_by('no desc');
		$query = $this->db->get_where('tbl_laporan', array('id_penyewa' => $id_penyewa));
		return $query->result();
	}
	
	// select - tampilan edit pembayaran pada biodata penyewa
	function readEditRiwayatPembayaran($id_penyewa, $id_laporan) {
		$query = $this->db->get_where('tbl_laporan', array('id_penyewa' => $id_penyewa, 'id_laporan' => $id_laporan));
		return $query->row();
	}
	
	// update - biodata penyewa
	function editData($id_penyewa) {
		$no_kamar_lama['kamar'] = $this->readPenyewa($id_penyewa);
		$no_kamar_baru = $this->input->post('no_kamar');
		
		if ($no_kamar_lama['kamar']->no_kamar != $no_kamar_baru) {
			$data = array(
				'status' => "kosong"
			);
			$this->db->where('no_kamar', $no_kamar_lama['kamar']->no_kamar);
			$this->db->update('tbl_kamar', $data);
			
			$data = array(
				'status' => "aktif"
			);
			$this->db->where('no_kamar', $no_kamar_baru);
			$this->db->update('tbl_kamar', $data);
		}
		
		$data = array(
			'no_kamar' => $no_kamar_baru,
			'nama' => $this->input->post('nama'),
			'asal' => $this->input->post('asal'),
			'no_hp' => $this->input->post('no_hp')
		);
		$this->db->where('id_penyewa', $id_penyewa);
		return $this->db->update('tbl_penyewa', $data);
	}
	
	// update - riwayat pembayaran
	function editPembayaran($id_laporan) {
		$data = array(
			'tgl_pembayaran' => $this->input->post('tgl_pembayaran'),
			'harga_sewa' => $this->input->post('harga_sewa')
		);
		$this->db->where('id_laporan', $id_laporan);
		return $this->db->update('tbl_laporan', $data);
	}
	
	// update - biodata penyewa, check out dari kosan
	function checkOut($id_penyewa, $no_kamar) {
		$data = array(
			'status' => "kosong",
			'harga_sewa' => 500000
		);
		$this->db->where('no_kamar', $no_kamar);
		$this->db->update('tbl_kamar', $data);
		
		//$no_kamar = $no_kamar;
		
		$data = array(
			'no_kamar' => $no_kamar." last",
			'tgl_keluar' => date('d M, Y')
		);
		$this->db->where('id_penyewa', $id_penyewa);
		return $this->db->update('tbl_penyewa', $data);
	}
	
	// select - untuk tampilan Pengaturan Kamar
	function listKamar() {
		$this->db->order_by('no_kamar asc');
		$query = $this->db->get('tbl_kamar');
		return $query->result_array();
	}
	
	// insert - tambah kamar pada Pengaturan Kamar
	function tambahKamar() {
		$data = array(
			'no_kamar' => $this->input->post('no_kamar'),
			'harga_sewa' => $this->input->post('harga_sewa')
		);
		return $this->db->insert('tbl_kamar', $data);
	}
	
	// update - untuk edit kamar pada Pengaturan Kamar
	function editKamar($id_kamar) {
		$data = array(
			'harga_sewa' => $this->input->post('harga_sewa')
		);
		$this->db->where('id_kamar', $id_kamar);
		return $this->db->update('tbl_kamar', $data);
	}
	
	// delete - hapus kamar pada Pengaturan Kamar
	function hapusKamar($id_kamar) {
		$this->db->where('id_kamar', $id_kamar);
		return $this->db->delete('tbl_kamar');
	}
}