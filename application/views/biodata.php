<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5>Biodata Penyewa</h5><hr>
		<p>
			No Kamar <span style="margin-left:2.5em"> : <?= $penyewa->no_kamar; ?></span><br>
			Nama  <span style="margin-left:4.3em"> : <?= $penyewa->nama; ?></span><br>
			Asal <span style="margin-left:5em"> : <?= $penyewa->asal; ?></span><br>
			No Hp <span style="margin-left:4.2em"> : <?= $penyewa->no_hp; ?></span><br>
			Tanggal Masuk <span style="margin-left:0.2em"> : <?= $penyewa->tgl_masuk; ?></span><br>
			Tanggal Keluar <span style="margin-left:0.4em"> : <?= $penyewa->tgl_keluar; ?></span><br>
		</p>
		<?php if (empty($penyewa->tgl_keluar)): ?>
			<a href="<?= site_url('biodata/editData/'.$penyewa->id_penyewa);?>" class="btn waves-effect grey">Edit Data</a>
			<div class="right">
				<form action="<?= site_url('biodata/checkout/'.$penyewa->id_penyewa);?>" method="post">
					<button class="btn waves-effect grey">Leave</button>
				</form>
			</div>
		<?php endif; ?>
	</div>
	<div class="card-panel z-depth-2">
		<h5>Riwayat Pembayaran</h5><hr>
		<table class="bordered">
			<tr>
				<th>Tanggal</th>
				<th>Harga Sewa</th>
				<th>Action</th>
			</tr>
			<?php foreach ($laporan as $laporan): ?>
			<tr>
				<td>
					<?= $laporan->tgl_pembayaran; ?>
				</td>
				<td>
					<?php
						if (strlen($laporan->harga_sewa) > 3) {
							$temp = substr_replace($laporan->harga_sewa, "", -3, 4);
							$harga = "Rp. ".substr_replace($laporan->harga_sewa, ".", -3, 0);
							if (strlen($temp) > 3) {
								$harga = substr_replace($harga, ".", -7, 0);
							}
						} else { $harga = "Rp. ".$laporan->harga_sewa;}
					?>
					<?= $harga; ?>
				</td>
				<td>
					<a href="<?= site_url('biodata/editPembayaran/'.$laporan->id_penyewa.'/'.$laporan->id_laporan);?>">Edit</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>