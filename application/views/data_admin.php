<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?= $this->session->flashdata('toast'); ?>
<div class="container" style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5 class="center">Edit Profil</h5>
		<?php foreach ($admin as $admin): ?>
			<?php echo form_open('account/index/'.$admin['username']); ?>
				<div class="row">
					<div class="input-field col l12 s12">
						<label for="nama">Nama</label>
						<input id="nama" name="nama" type="text" value="<?= $admin['name'];?>" pattern="[A-Za-z ]*" required>
					</div>
					<div class="input-field col l12 s12">
						<label for="username">Username</label>
						<input id="username" name="username" type="text" value="<?= $admin['username'];?>" pattern="[A-Za-z_]*" required>
					</div>
					<div class="input-field col l12 s12">
						<label for="email">Email</label>
						<input id="email" name="email" type="email" value="<?= $admin['email'];?>" required>
					</div>
				</div>
				<div style="text-align: right;">
					<button type="submit" class="btn waves-effect grey">Kirim</button>
				</div>
			<?php echo form_close(); ?>
		<?php endforeach; ?>
	</div>
	<div class="card-panel z-depth-2">
		<h5 class="center">Ganti Password</h5>
		<?php echo form_open('account/editPassword'); ?>
			<div class="row">
				<div class="input-field col l12 s12">
					<label for="password_lama">Password Lama</label>
					<input id="password_lama" name="password_lama" type="password" required>
				</div>
				<div class="input-field col l12 s12">
					<label for="password_baru">Password Baru</label>
					<input id="password_baru" name="password_baru" type="password" required>
				</div>
				<div class="input-field col l12 s12">
					<label for="konfirmasi_password_baru">Konfirmasi Password Baru</label>
					<input id="konfirmasi_password_baru" name="konfirmasi_password_baru" type="password" required>
				</div>
			</div>
			<div style="text-align: right;">
				<button type="submit" class="btn waves-effect grey">Kirim</button>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>