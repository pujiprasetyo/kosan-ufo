<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0" class="center">
	<div class="card-panel z-depth-2">
		<h4>Verifikasi Pembayaran</h4><hr>
		<h5>
			<ul>No. Kamar : <?= $penyewa->no_kamar; ?></ul>
			<ul><?= $penyewa->nama; ?></ul>
			<ul><?= $penyewa->no_hp; ?></ul>
		</h5>
		<?php echo form_open('site/verifikasi/'.$penyewa->id_penyewa); ?>
			<div class="row">
				<div class="col s12">
					<?php
						if (strlen($harga_sewa->harga_sewa) > 3) {
							$temp = substr_replace($harga_sewa->harga_sewa, "", -3, 4);
							$harga = "Rp. ".substr_replace($harga_sewa->harga_sewa, ".", -3, 0);
							if (strlen($temp) > 3) {
								$harga = substr_replace($harga, ".", -7, 0);
							}
						} else { $harga = "Rp. ".$harga_sewa->harga_sewa;}
					?>
					Telah melakukan pembayaran sebesar <big><?= $harga; ?></big> pada tanggal :
					<div class="input-field inline">
						<label for="tanggal">Tanggal</label>
						<input id="tanggal" name="tanggal" type="text" class="datepicker">
						<?php echo form_error('tanggal'); ?>
					</div>
				</div>
			</div>
			<a href="<?= site_url('');?>" class="btn waves-effect grey">Batal</a>
			&nbsp;&nbsp;
			<button type="submit" class="btn waves-effect grey">Kirim</button>
		<?php echo form_close(); ?>
	</div>
</div>
<script type="text/javascript" src="<?= site_url('asset/js/inputTanggal.js'); ?>"></script>