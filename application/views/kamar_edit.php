<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
	<div class="row">
		<div class="col l6 s6">
			<h5>Edit Kamar</h5>
		</div>
		<div class="col l6 s6">
			<div class="right">
				<a href="<?= site_url('kamar/hapusKamar/'.$kamar->id_kamar);?>" title="Hapus kamar"><h5><i class="material-icons">delete_forever</i></h5></a>
			</div>
		</div>
		<hr>
	</div>
	
	<?php echo form_open('kamar/editKamar/'.$kamar->id_kamar.'/'.$kamar->no_kamar); ?>
		<div class="row">
			<div class="col l6 s12">
				<label for="no_kamar">No Kamar</label>
				<input disabled id="no_kamar" name="no_kamar" type="number" value="<?= $kamar->no_kamar?>">
			</div>
			<div class="col l6 s12">
				<label for="harga_sewa">Harga Sewa</label>
				<input id="harga_sewa" name="harga_sewa" type="number" class="validate" value="<?= $kamar->harga_sewa?>" required>
			</div>
		</div>
		<a href="<?= site_url('kamar/');?>" class="btn waves-effect grey">Batal</a>
		&nbsp;&nbsp;
		<button type="submit" class="btn waves-effect grey">Kirim</button>
		
	<?php echo form_close(); ?>
	</div>
</div>