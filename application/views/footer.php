<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</main>
<footer class=" page-footer grey lighten-4">
	<div class="footer-copyright grey darken-4">
		<div class="container">
			<a class="grey-text text-lighten-4 left">&copy; 2018 Pengelolaan Proyek Perangkat Lunak</a>
			<a href="#" class="grey-text text-lighten-4 right" data-activates="goto-top" title="Go to top">4IA16</a>
		</div>
	</div>
</footer>

<style>
	body {
		display: flex;
		min-height: 100vh;
		flex-direction: column;
	}
	main {
		flex: 1 0 auto;
	}
	.delimiter {
		background-color: #FFB6C1;
	}
	#toast-container {
		min-width: 20%;
		bottom: 15%;
		margin-left: 5%;
	}
</style>

<script>
	$('.button-collapse').sideNav(); // untuk navigasi versi mobile
	$('select').material_select(); // untuk yang bulan dan tahun dropdown
	$('.dropdown-button').dropdown({ // menu dropdown
		constrainWidth: true, // Does not change width of dropdown to that of the activator
		//hover: true, // Activate on hover
		gutter: 0, // Spacing from edge
		belowOrigin: true, // Displays dropdown below the button
		alignment: 'left', // Displays dropdown with edge aligned to the left of button
		stopPropagation: true // Stops event propagation
	});
</script>

</body>
</html>