<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5>Biodata Penyewa - Perubahan</h5><hr>
		<?php echo form_open('biodata/editData/'.$penyewa->id_penyewa); ?>
			<div class="row">
				<div class="col l6 s12">
					<label for="tgl_masuk">Tanggal Masuk</label>
					<input disabled id="tgl_masuk" name="tgl_masuk" type="text" value="<?= $penyewa->tgl_masuk; ?>">
				</div>
				<div class="col l6 s12">
					<label for="tgl_keluar">Tanggal Keluar</label>
					<input disabled id="tgl_keluar" name="tgl_keluar" type="text" value="<?= $penyewa->tgl_keluar; ?>">
				</div>
				<div class="col l6 s12">
					<label for="no_kamar">No Kamar <span style="margin-left:2.3em"> : </span></label>
					<select id="no_kamar" name="no_kamar" class="browser-default">
						<option value="<?= $penyewa->no_kamar; ?>"><?= $penyewa->no_kamar; ?></option>
						<?php foreach ($kamar as $kamar): ?>
							<option value="<?= $kamar['no_kamar']; ?>"><?= $kamar['no_kamar']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col l6 s12">
					<label for="nama">Nama</label>
					<input id="nama" name="nama" type="text" value="<?= $penyewa->nama; ?>" pattern="[A-Za-z ]*" required>
				</div><br>
				<div class="col l6 s12">
					<label for="asal">Asal</label>
					<input id="asal" name="asal" type="text" value="<?= $penyewa->asal; ?>" pattern="[A-Za-z ]*" required>
				</div>
				<div class="col l6 s12">
					<label for="no_hp">No Hp</label>
					<input id="no_hp" name="no_hp" type="number" value="<?= $penyewa->no_hp; ?>" required>
				</div>
			</div>
			<a href="<?= site_url('biodata/penyewa/'.$penyewa->id_penyewa);?>" class="btn waves-effect grey">Batal</a>
			&nbsp;&nbsp;
			<button type="submit" class="btn waves-effect grey">Kirim</button>
		<?php echo form_close(); ?>
	</div>
</div>