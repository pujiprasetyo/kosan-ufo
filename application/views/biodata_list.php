<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5 class="center">Daftar Penyewa</h5>
		<table class="responsive-table striped">
			<tr class="blue lighten-4">
				<th>No Kamar</th>
				<th>Nama</th>
				<th>Asal</th>
				<th>No Hp</th>
				<th>Tanggal Masuk</th>
				<th>Tanggal Keluar</th>
			</tr>
			<?php foreach ($penyewa as $penyewa): ?>
				<tr>
					<td><?= $penyewa['no_kamar'];?></td>
					<td><a href="<?= site_url('biodata/penyewa/'.$penyewa['id_penyewa']);?>" style="color: black"><?= $penyewa['nama'];?></a></td>
					<td><?= $penyewa['asal'];?></td>
					<td><?= $penyewa['no_hp'];?></td>
					<td><?= $penyewa['tgl_masuk'];?></td>
					<td><?= $penyewa['tgl_keluar'];?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>