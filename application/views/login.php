<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?= $this->session->flashdata('toast'); ?>
<div class="container" style="padding: 15px;">
	<div class="card-panel z-depth-3">
		<form action="<?= site_url('auth/index')?>" method="post">
			<p>
				<h5 class="center">Kosan UFO</h5>
			</p>
			<big>Username or Email</big>
				<input id="username" name="username" type="text" placeholder="Type here" required autofocus/>
			<big>Password</big>
				<input id="password" name="password" type="password" placeholder="Type here" required/>
			<center>
				<button name="login" class="center btn waves-effect grey">Login</button>
				<p class="center">
					<small>
						<a href="<?= site_url('resetpassword');?>">Forgot password</a>
					</small>
				</p>
			</center>
		</form>
	</div>
</div>