<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
	<h5>Riwayat Pembayaran - Perubahan</h5><hr>
	<?php echo form_open('biodata/editPembayaran/'.$laporan->id_penyewa.'/'.$laporan->id_laporan); ?>
		<div class="row">
			<div class="col l6 s12">
				<label for="tgl_pembayaran">Tanggal Masuk</label>
				<input id="tgl_pembayaran" name="tgl_pembayaran" type="text" class="datepicker" value="<?= $laporan->tgl_pembayaran; ?>">
			</div>
			<div class="col l6 s12">
				<label for="harga_sewa">Harga Sewa</label>
				<input id="harga_sewa" name="harga_sewa" type="number" value="<?= $laporan->harga_sewa; ?>" required>
			</div>
			<div class="col l5 s12">
				<?php echo form_error('tgl_pembayaran'); ?>
			</div>
		</div>
		<a href="<?= site_url('biodata/penyewa/'.$laporan->id_penyewa);?>" class="btn waves-effect grey">Batal</a>
		&nbsp;&nbsp;
		<button type="submit" class="btn waves-effect grey">Kirim</button>
	<?php echo form_close(); ?>
	</div>
</div>
<script type="text/javascript" src="<?= site_url('asset/js/inputTanggal.js'); ?>"></script>