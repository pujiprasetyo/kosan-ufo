<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Kosan UFO</title>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<!--Theme color untuk Chrome, Firefox OS, Opera & Vivaldi-->
	<meta name="theme-color" content="#006000">
	<!--Theme color untuk iOS Safari-->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="#006000">
	<link rel="stylesheet" type="text/css" href="<?= site_url('asset/css/materialize.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= site_url('asset/fonts/material-icons/material-icons.css'); ?>">
	<script type="text/javascript" src="<?= site_url('asset/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?= site_url('asset/js/materialize.min.js'); ?>"></script>
</head>

<body class="grey lighten-4">
	<div class="navbar-fixed">
		<nav class="green darken-1">
			<?php if (!$this->session->has_userdata('username')):?>
				<div class="center">
					<big>Aplikasi Tagihan Sewa Kosan</big>
				</div>
			<?php else:?>
				<a href="#" class="button-collapse" data-activates="mobile-menu">
					<i class="material-icons">menu</i>
				</a>
				<div class="container nav-wrapper">
					<ul class="show-on-small">
						<li><a href="<?= site_url('site'); ?>">Catatan Sewa Kosan</a></li>
					</ul>
					<ul class="hide-on-med-and-down">
						<li><a href="<?= site_url('biodata'); ?>">Daftar Penyewa</a></li>
						<li><a href="<?= site_url('kamar'); ?>">Pengaturan Kamar</a></li>
						<li class="right">
							<a class="right dropdown-button" data-activates="dropdown1" data-beloworigin="true">
								<i class="material-icons left">account_circle</i>
									<?= $this->session->userdata('username'); ?>
								<i class="material-icons right">arrow_drop_down</i>
							</a>
						</li>
						<ul id='dropdown1' class='dropdown-content'>
							<li><a href="<?= site_url('account'); ?>">Edit Profil</a></li>
							<li><a href="<?= site_url('account/logout'); ?>">Keluar</a></li>
						</ul>
					</ul>
				</div>
			<?php endif;?>
		</nav>
	</div>
	<ul class="side-nav grey lighten-4" id="mobile-menu">
		<div class="center green darken-1" style="padding: 10px">
			<h5>Aplikasi Tagihan Sewa Kosan</h5>
		</div>
		<li><a href="<?= site_url('biodata'); ?>">Daftar Penyewa</a></li>
		<li><a href="<?= site_url('kamar'); ?>">Pengaturan Kamar</a></li>
		<li><a href="<?= site_url('account'); ?>">Edit Profil</a></li>
		<li class="divider"></li>
		<li><a href="<?= site_url('account/logout'); ?>">Keluar</a></li>
	</ul>

<!--Untuk custom containetr ~ style="width: 90%"-->
<main class="container" id="goto-top">
