<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?= $this->session->flashdata('toast'); ?>
<div class="container center" style="padding: 15px;">
	<div class="card-panel z-depth-2">
		<h4>Reset Password</h4>
		<?php echo form_open('resetpassword'); ?>
			<div class="row">
				<div class="input-field col s12">
					<label for="email">Masukkan Alamat Email</label>
					<input id="email" name="email" type="email" class="validate" required autofocus>
					<?php echo form_error('email'); ?>
				</div>
			</div>
			<a href="<?= site_url('');?>" class="btn waves-effect grey">Batal</a>
			&nbsp;&nbsp;
			<button type="submit" class="btn waves-effect grey">Kirim</button>
		<?php echo form_close(); ?>
	</div>
</div>