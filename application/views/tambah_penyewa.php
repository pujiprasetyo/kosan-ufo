<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0" class="center">
	<div class="card-panel z-depth-2">
		<h4>Form Penyewa Baru</h4>
		<?php echo form_open('site/tambahPenyewa/'.$kamar->no_kamar); ?>
			<div class="row">
				<div class="input-field col l4 s12">
					<label for="tgl_masuk">Tanggal masuk</label>
					<input disabled id="tgl_masuk" name="tgl_masuk" type="text" value="<?=  date('d M, Y');?>">
				</div>
				<div class="input-field col l4 s6">
					<label for="no_kamar">No Kamar</label>
					<input disabled id="no_kamar" name="no_kamar" type="number" value="<?= $kamar->no_kamar; ?>">
				</div>
				<div class="input-field col l4 s6">
					<label for="harga_sewa">Harga Sewa</label>
					<input id="harga_sewa" name="harga_sewa" type="text" value="<?= $kamar->harga_sewa; ?>">
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s12">
					<label for="nama">Nama</label>
					<input id="nama" name="nama" type="text" class="validate" pattern="[A-Za-z ]*" required>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s6">
					<label for="asal">Asal</label>
					<input id="asal" name="asal" type="text" class="validate" pattern="[A-Za-z ]*" required>
				</div>
				<div class="input-field col s6">
					<label for="no_hp">No Hp</label>
					<input id="no_hp" name="no_hp" type="number" class="validate" required>
				</div>
			</div>
			<a href="<?= site_url('');?>" class="btn waves-effect grey">Batal</a>
			&nbsp;&nbsp;
			<button type="submit" class="btn waves-effect grey">Kirim</button>
		<?php echo form_close(); ?>
	</div>
</div>