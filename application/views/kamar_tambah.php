<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
	<h5>Tambah Kamar</h5><hr>
	<?php echo form_open('kamar/tambahKamar'); ?>
		<div class="row">
			<div class="col l6 s12">
				<label for="no_kamar">No Kamar</label>
				<input id="no_kamar" name="no_kamar" type="number" class="validate" required>
			</div>
			<div class="col l6 s12">
				<label for="harga_sewa">Harga Sewa</label>
				<input id="harga_sewa" name="harga_sewa" type="number" class="validate" required>
			</div>
			<div class="col l4 s12">
				<?php echo form_error('no_kamar'); ?>
			</div>
		</div>
		<a href="<?= site_url('kamar/');?>" class="btn waves-effect grey">Batal</a>
		&nbsp;&nbsp;
		<button type="submit" class="btn waves-effect grey">Kirim</button>
	<?php echo form_close(); ?>
	</div>
</div>