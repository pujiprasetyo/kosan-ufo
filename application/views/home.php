<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5 class="center">Daftar Kamar</h5>
		<table class="responsive-table striped">
			<tr class="blue lighten-4">
				<th>Kamar</th>
				<th>Nama</th>
				<th>No Hp</th>
				<th>Pemabayaran Terakhir</th>
				<th>Action</th>
			</tr>
			<?php foreach ($penyewa as $penyewa): ?>
				<tr>
					<td><big><?= $penyewa['no_kamar'];?></big></td>
					<td><a href="<?= site_url('biodata/penyewa/'.$penyewa['id_penyewa']);?>" style="color: black"><?= $penyewa['nama'];?></a></td>
					<td><?= $penyewa['no_hp'];?></td>
					<td><?= $penyewa['tgl_pembayaran'];?></td>
					
					<td>
						<?php if ($penyewa['nama'] == null):?>
							<a href="<?= site_url('site/tambahPenyewa/'.$penyewa['no_kamar']);?>">Tambah</a>
						<?php else:?>
							<a href="<?= site_url('site/verifikasi/'.$penyewa['id_penyewa']);?>">Bayar</a>
						<?php endif;?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>