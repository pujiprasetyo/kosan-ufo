<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="card-panel z-depth-2">
		<h5 class="center">Pengaturan Kamar</h5>
		<div class="row center">
			<?php foreach ($kamar as $kamar): ?>
			<div class="col l3 s6 " style="padding:10px 0 10px 0;">
				<div class="z-depth-2" style="padding:30px 0 30px 0; margin:0px 5px 0px 5px; border:1px solid;">
					<p>No Kamar<br><br>
						<a href="<?= site_url('kamar/editKamar/'.$kamar['id_kamar'].'/'.$kamar['no_kamar']);?>" title="Edit kamar" class="btn-floating btn-large green">
							<big><?= $kamar['no_kamar'];?></big>
						</a><br><br>
						<?php
							if (strlen($kamar['harga_sewa']) > 3) {
								$temp = substr_replace($kamar['harga_sewa'], "", -3, 4);
								$harga = "Rp. ".substr_replace($kamar['harga_sewa'], ".", -3, 0);
								if (strlen($temp) > 3) {
									$harga = substr_replace($harga, ".", -7, 0);
								}
							} else { $harga = "Rp. ".$kamar['harga_sewa'];}
						?>
						<?= $harga;?>
					</p>
				</div>
			</div>
			<?php endforeach; ?>
			<div class="col l3 s6" style="padding:10px 0 10px 0;">
				<div  style="padding:30px 0 30px 0; margin:0px 5px 0px 5px; border:2px dashed;">
					<p class="white-text"><br><br>
						<a href="<?= site_url('kamar/tambahKamar');?>" title="Tambah kamar" class="btn-floating btn-large waves-effect red">
							<i class="material-icons">add</i>
						</a><br><br><br>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>