// inisialisasi variabel untuk pickdate
var input_pickadate = $('.datepicker').pickadate({
	format: 'dd mmmm, yyyy'
});

// the picker

var picker = $(input_pickadate).pickadate('picker');

input_pickadate.on('click', function(e){
	$(picker).data('stay_open', true );
	picker.open();
});

// auto reopen is disallowed
picker.on('open', function(){
	if( !$(picker).data('stay_open') ){
		picker.close();
	}
});

picker.on('close', function(){
	$(picker).data('stay_open', false );
});
